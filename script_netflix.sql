DROP DATABASE IF EXISTS bdexerc29;
CREATE DATABASE bdexerc29;
USE bdexerc29;

create table usuario(
	idusuario int not null primary key auto_increment 
    , nome varchar(45)
	, email varchar(45)
);

create table episodio(
	idepisodio int not null
    , nome varchar(45)
    , temporada int
    , numero int
    , duracao time
    , primary key (idepisodio)
);

create table acesso(
	idacesso int not null primary key auto_increment
    , idusuario int not null
    , idepisodio int not null
    , dataacesso date
    , horaacesso time
    , tempoacesso time
    , constraint fk_acesso_episodio foreign key (idepisodio) references 
    episodio(idepisodio) on delete cascade on update cascade
    , constraint fk_acesso_usuario foreign key (idusuario) references 
    usuario(idusuario) on delete cascade on update cascade
);

INSERT INTO usuario VALUES ( 1, 'Maria Helena', 'maria@internet.com');
INSERT INTO usuario VALUES ( 2, 'Carlos Alberto', 'carlo@internet.com');
INSERT INTO usuario VALUES ( 3, 'João da Silva', 'joao@internet.com');
INSERT INTO episodio VALUES ( 1, 'Piloto', 1, 1, 5500);
INSERT INTO episodio VALUES ( 2, 'Piloto', 1, 2, 5000);
INSERT INTO episodio VALUES ( 3, 'Piloto', 1, 3, 4700);
INSERT INTO episodio VALUES ( 4, 'Piloto', 2, 4, 5800);
INSERT INTO episodio VALUES ( 5, 'Piloto', 2, 5, 4900);
INSERT INTO episodio VALUES ( 6, 'Piloto', 2, 6, 5900);
INSERT INTO acesso VALUES ( 1,1,1, '2016-06-18', 2000, 5000);
INSERT INTO acesso VALUES ( 2,1,2, '2016-06-20',1930, 4900);
INSERT INTO acesso VALUES ( 3,2,1, '2016-06-20', 2100, 3200);
INSERT INTO acesso VALUES ( 4,3,1, '2016-07-01', 2145, 5000);
INSERT INTO acesso VALUES ( 5,2,5, '2016-06-20', 2109, 4900);
INSERT INTO acesso VALUES ( 6,1,3, '2016-06-20', 1923, 4600);
INSERT INTO acesso VALUES ( 7,3,2, '2016-07-01', 2141, 4900);
INSERT INTO acesso VALUES ( 8,1,4, '2016-06-20', 2025, 5800);
INSERT INTO acesso VALUES ( 9,1,6, '2016-06-20', 2130, 5500);
INSERT INTO acesso VALUES ( 10,1,5, '2016-06-21', 1902, 4900);

/*1.A empresa de produção precisa de uma consulta com todos os dados
armazenados sobre os acessos no mês de julho de 2016, sendo assim crie uma consulta
SQL que liste o código identificador do usuário, o nome do usuário, o código identificador
do acesso, a data do acesso, a hora do acesso, o código identificador do episódio, o
nome do episódio, o número do episódio e a temporada do episódio. Liste apenas os
acessos entre 01/07/2016 e 31/07/2016, ordenado pelo nome do usuário em seguida
pelo código identificador do episódio, para ordenar utilize a cláusula “ORDER BY”.*/

select *from episodio;

select
	usuario.idusuario
    , usuario.nome
    , acesso.idacesso
    , acesso.dataacesso
    , acesso.horaacesso
    , acesso.horaacesso
    , episodio.idepisodio
    , episodio.nome
    , episodio.numero
    , episodio.temporada
from
	usuario as usuario
    inner join acesso on
    acesso.idacesso = usuario.idusuario
    inner join episodio on
    episodio.idepisodio = acesso.idacesso
where
	acesso.dataacesso
;

/*2.A empresa deseja saber qual o maior número de episódio assistido pelo
usuário, sendo assim crie uma consulta para listar o código identificador do usuário, o
nome do usuário, o e-mail do usuário, e o maior código identificador de episódio assistido
pelo usuário, mesmo que o usuário não tenha assistido nenhum episódio ele deve vir no
resultado da consulta.*/

select
	usuario.idusuario
    , usuario.nome
    , usuario.email
    , acesso.idacesso
    , max(tempoacesso)
from
	usuario as usuario
    inner join acesso on
    acesso.idacesso = usuario.idusuario
    inner join episodio on
    episodio.idepisodio = acesso.idacesso
order by
	episodio.idepisodio
;

/*3.A empresa precisa identificar quanto tempo tem cada temporada baseada na
duração dos episodios, sendo assim crie uma consulta para listar a temporada e a soma
dos minutos de cada episódio.*/

select 
episodio.duracao
, episodio.temporada
, count(episodio.duracao)
from 
	episodio
group by
	episodio.duracao
;

/*4.(Desafio – sem usar subquery) Crie uma consulta para o nome e e-mail dos
usuários que assistiram todos os episódios, apenas os usuários que tem acesso em
todos os episódios.*/
/*não deu tempo*/

/*5.Crie uma consulta para listar a temporada, a quantidade de episódios,
quantidade de acesso, quantidade de usuário, agrupado por temporada. O resultado
deve ser apresentado como a tabela abaixo (Tabela 1).*/

select
    episodio.temporada
	, count(episodio.temporada)
    , sum(acesso.idacesso)
    , count(acesso.idusuario)
from
	episodio
    inner join acesso on
    episodio.idepisodio = acesso.idacesso
    inner join usuario on
    acesso.idacesso = usuario.idusuario
group by
	episodio.temporada
; 



